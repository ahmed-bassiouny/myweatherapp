package com.bassiouny.component_network

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject


class OkHttpInterceptor @Inject constructor(
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val url: HttpUrl = request.url.newBuilder().addAPIKey().build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }

    private fun HttpUrl.Builder.addAPIKey(): HttpUrl.Builder =
        this.addQueryParameter("key",BuildConfig.API_KEY)
}