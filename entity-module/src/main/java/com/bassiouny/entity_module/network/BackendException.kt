package com.bassiouny.entity_module.network

data class BackendException(override val message: String): Exception(message) {
    data class InternetConnection(override val message: String): Exception(message)
    data class RequestTimeout(override val message: String): Exception(message)
    data class Unauthorized(override val message: String): Exception(message)
}