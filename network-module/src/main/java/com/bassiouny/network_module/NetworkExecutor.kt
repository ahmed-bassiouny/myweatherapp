package com.bassiouny.network_module

interface NetworkExecutor {
    suspend fun <RESPONSE>execute(apiCall: suspend () -> RESPONSE): RESPONSE
}