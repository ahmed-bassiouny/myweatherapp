package com.bassiouny.network_module

import com.bassiouny.entity_module.network.BackendException
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class AppNetworkExecutor @Inject constructor() : NetworkExecutor {

    override suspend fun <T> execute(apiCall: suspend () -> T): T = try {
        apiCall.invoke()
    } catch (exception: Exception) {
        throw wrappedExceptionOf(exception)
    }

    private fun wrappedExceptionOf(from: Exception): Exception = when (from) {
        is HttpException -> wrappedHttpExceptionOf(from)
        is SocketTimeoutException -> BackendException.RequestTimeout(from.message.toString())
        is UnknownHostException -> BackendException.InternetConnection(from.message.toString())
        else -> from
    }

    private fun wrappedHttpExceptionOf(from: HttpException) = when {
        from.code() == 401 -> BackendException.Unauthorized(from.message())
        else -> BackendException(from.message())
    }
}