package com.bassiouny.network_module.weather_network

import com.bassiouny.entity_module.weather.ForecastResponse

interface WeatherNetwork {

    suspend fun getWeatherForecast(city: String,days:Int): ForecastResponse
    suspend fun getWeatherForecast(lat:Double,long:Double,days:Int): ForecastResponse
}