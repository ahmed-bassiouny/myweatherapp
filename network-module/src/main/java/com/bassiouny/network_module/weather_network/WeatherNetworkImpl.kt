package com.bassiouny.network_module.weather_network

import com.bassiouny.entity_module.weather.ForecastResponse
import com.bassiouny.network_module.NetworkExecutor
import com.bassiouny.network_module.weather.WeatherApi
import javax.inject.Inject

class WeatherNetworkImpl @Inject constructor(
    private val weatherApi: WeatherApi,
    private val apiExecutor: NetworkExecutor
) : WeatherNetwork {
    override suspend fun getWeatherForecast(city: String,days:Int): ForecastResponse =
        apiExecutor.execute {
            weatherApi.getWeatherForecast(city = city, days)
        }

    override suspend fun getWeatherForecast(lat: Double, long: Double,days:Int): ForecastResponse =
        apiExecutor.execute {
            weatherApi.getWeatherForecast(city = "$lat,$long", days)
        }
}