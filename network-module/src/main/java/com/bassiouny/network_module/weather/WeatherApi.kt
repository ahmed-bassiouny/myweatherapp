package com.bassiouny.network_module.weather

import com.bassiouny.entity_module.weather.ForecastResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("forecast.json")
    suspend fun getWeatherForecast(
        @Query("q") city: String,
        @Query("days") days: Int ,
    ): ForecastResponse
}