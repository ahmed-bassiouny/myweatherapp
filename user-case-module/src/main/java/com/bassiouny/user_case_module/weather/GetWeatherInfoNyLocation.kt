package com.bassiouny.user_case_module.weather

import com.bassiouny.entity_module.user.UserLocation
import com.bassiouny.entity_module.weather.ForecastResponse
import com.bassiouny.repository_module.weather.DefaultWeatherRepository
import com.bassiouny.user_case_module.UseCase

import javax.inject.Inject

class GetWeatherInfoNyLocation @Inject constructor(
    private val defaultWeatherRepository: DefaultWeatherRepository
) : UseCase<UserLocation, UseCase.Response<ForecastResponse>> {

    override suspend fun execute(request: UserLocation) = wrap {
        defaultWeatherRepository.getWeatherForecast(request.lat,request.long,10)
    }

}
