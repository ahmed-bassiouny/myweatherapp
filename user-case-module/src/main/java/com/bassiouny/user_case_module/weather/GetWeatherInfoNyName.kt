package com.bassiouny.user_case_module.weather

import com.bassiouny.entity_module.weather.ForecastResponse
import com.bassiouny.repository_module.weather.DefaultWeatherRepository
import com.bassiouny.user_case_module.UseCase
import javax.inject.Inject

class GetWeatherInfoNyName @Inject constructor(
    private val defaultWeatherRepository: DefaultWeatherRepository
) : UseCase<String, UseCase.Response<ForecastResponse>> {

    override suspend fun execute(request: String) = wrap {
        defaultWeatherRepository.getWeatherForecast(request, 10)
    }

}

const val DEFAULT_WEATHER_DESTINATION = "Alexandria"
