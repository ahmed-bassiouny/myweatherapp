package com.bassiouny.user_case_module

import android.util.Log
import com.bassiouny.entity_module.network.BackendException

interface UseCase<REQUEST, RESPONSE> {
    suspend fun execute(request: REQUEST): RESPONSE


    suspend fun <T> wrap(block: suspend () -> T): Response<T> = try {
        Response.Success(block.invoke())
    } catch (internetConnectionException: com.bassiouny.entity_module.network.BackendException.InternetConnection) {
        Response.Error.Network(internetConnectionException.message)
    } catch (requestTimeoutException: com.bassiouny.entity_module.network.BackendException.RequestTimeout) {
        Response.Error.Network(requestTimeoutException.message)
    } catch (backendException: com.bassiouny.entity_module.network.BackendException) {
        Response.Error.Backend(backendException.message)
    } catch (exception: Exception) {
        Log.e(javaClass.toString(), exception.message.toString())
        Response.Error.Generic(exception.message.toString())
    }

    sealed class Response<T> {
        data class Success<T>(val value: T): Response<T>()
        sealed class Error<T>(open val error: String): Response<T>() {
            data class Backend<T>(override val error: String) : Error<T>(error)
            data class Generic<T>(override val error: String) : Error<T>(error)
            data class Network<T>(override val error: String) : Error<T>(error)
        }
    }
}