package com.bassiouny.user_case_module.location

import android.location.Location
import com.bassiouny.repository_module.location.LocationRepository
import com.bassiouny.user_case_module.UseCase
import javax.inject.Inject


class LocationOnRequest @Inject constructor(
    private val locationRepository: LocationRepository,
) : UseCase<Unit, Location?> {
    override suspend fun execute(request: Unit): Location? = locationRepository.getLastKnownLocation()

}