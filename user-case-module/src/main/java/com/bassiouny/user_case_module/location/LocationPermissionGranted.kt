package com.bassiouny.user_case_module.location

import com.bassiouny.repository_module.location.LocationRepository
import com.bassiouny.user_case_module.UseCase
import javax.inject.Inject

class LocationPermissionGranted @Inject constructor(
    private val locationRepository: LocationRepository,
) : UseCase<Unit, Boolean> {

    override suspend fun execute(request: Unit): Boolean =
        locationRepository.hasLocationPermission() && locationRepository.hasBackgroundLocationPermission()

}
