package com.bassiouny.repository_module.location

import android.location.Location
import com.bassiouny.dao_module.location.LocationStorage
import javax.inject.Inject

class UserLocationRepository @Inject constructor(
    private val locationStorage: LocationStorage,
) : LocationRepository {

    override suspend fun hasLocationPermission(): Boolean =
        locationStorage.hasLocationPermission()

    override suspend fun hasLocationEnabled(): Boolean =
        locationStorage.hasLocationEnabled()

    override suspend fun hasBackgroundLocationPermission(): Boolean =
        locationStorage.hasBackgroundPermission()

    override suspend  fun getLastKnownLocation(): Location? = locationStorage.getLastKnownLocation()
}