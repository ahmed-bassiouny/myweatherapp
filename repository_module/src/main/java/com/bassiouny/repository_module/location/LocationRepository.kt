package com.bassiouny.repository_module.location

import android.location.Location

interface LocationRepository {
    suspend fun hasLocationPermission(): Boolean
    suspend fun hasLocationEnabled(): Boolean
    suspend fun hasBackgroundLocationPermission(): Boolean
    suspend fun getLastKnownLocation(): Location?
}