package com.bassiouny.repository_module.weather

import com.bassiouny.entity_module.weather.ForecastResponse

interface WeatherRepository {
    suspend fun getWeatherForecast(city: String,days:Int): ForecastResponse
    suspend fun getWeatherForecast(lat:Double,long:Double,days:Int): ForecastResponse
}