package com.bassiouny.repository_module.weather

import com.bassiouny.entity_module.weather.ForecastResponse
import com.bassiouny.network_module.weather_network.WeatherNetwork
import javax.inject.Inject

class DefaultWeatherRepository @Inject constructor(
    private val weatherNetwork: WeatherNetwork
) : WeatherRepository {

    override suspend fun getWeatherForecast(city: String,days:Int): ForecastResponse =
        weatherNetwork.getWeatherForecast(city,days)

    override suspend fun getWeatherForecast(lat:Double,long:Double,days:Int): ForecastResponse =
        weatherNetwork.getWeatherForecast(lat, long, days)

}
