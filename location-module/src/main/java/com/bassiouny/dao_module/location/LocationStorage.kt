package com.bassiouny.dao_module.location

import android.location.Location

interface LocationStorage {
    fun hasLocationPermission(): Boolean
    fun hasBackgroundPermission(): Boolean
    fun hasLocationEnabled(): Boolean
    suspend fun getLastKnownLocation(): Location?
}