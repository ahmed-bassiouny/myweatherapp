package com.bassiouny.dao_module.location

import dagger.hilt.android.qualifiers.ApplicationContext
import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.core.location.LocationManagerCompat
import com.bassiouny.dao_module.locationManager
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class UserLocationStorage @Inject constructor(
    @ApplicationContext private val applicationContext: Context,
) : LocationStorage {

    override fun hasLocationPermission(): Boolean =
        hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    override fun hasBackgroundPermission(): Boolean =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            hasPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        else
            hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    override fun hasLocationEnabled(): Boolean =
        LocationManagerCompat.isLocationEnabled(applicationContext.locationManager())


    private fun hasPermission(permission: String) = ContextCompat.checkSelfPermission(
        applicationContext, permission
    ) == PackageManager.PERMISSION_GRANTED


    @SuppressLint("MissingPermission")
    override suspend fun getLastKnownLocation():Location? = try {
        LocationServices.getFusedLocationProviderClient(applicationContext)
            .lastLocation.await()
    } catch (e: Exception) {
        null
    }

}
