package com.bassiouny.weatherapp.di

import android.content.Context
import com.bassiouny.dao_module.location.LocationStorage
import com.bassiouny.dao_module.location.UserLocationStorage
import com.bassiouny.network_module.NetworkExecutor
import com.bassiouny.network_module.weather.WeatherApi
import com.bassiouny.network_module.weather_network.WeatherNetwork
import com.bassiouny.network_module.weather_network.WeatherNetworkImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocationModule {


    @Singleton
    @Provides
     fun provideLocationStorage(
        @ApplicationContext applicationContext: Context
    ):LocationStorage = UserLocationStorage(applicationContext)

}