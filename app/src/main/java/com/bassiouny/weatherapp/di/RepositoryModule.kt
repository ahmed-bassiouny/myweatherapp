package com.bassiouny.weatherapp.di

import com.bassiouny.network_module.weather_network.WeatherNetwork
import com.bassiouny.dao_module.location.LocationStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.bassiouny.repository_module.weather.DefaultWeatherRepository
import com.bassiouny.repository_module.location.LocationRepository
import com.bassiouny.repository_module.location.UserLocationRepository
import com.bassiouny.repository_module.weather.WeatherRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(weatherNetwork: WeatherNetwork): WeatherRepository =
        DefaultWeatherRepository(
            weatherNetwork
        )

    @Singleton
    @Provides
    fun provideLocationRepository(locationStorage: LocationStorage): LocationRepository =
        UserLocationRepository(locationStorage)

}