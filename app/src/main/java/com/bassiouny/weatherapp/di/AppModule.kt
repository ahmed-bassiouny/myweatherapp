package com.bassiouny.weatherapp.di

import com.bassiouny.dao_module.location.LocationStorage
import com.bassiouny.dao_module.location.UserLocationStorage
import com.bassiouny.network_module.AppNetworkExecutor
import com.bassiouny.network_module.NetworkExecutor
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun bindApiExecutor(
        executor: AppNetworkExecutor
    ): NetworkExecutor

}