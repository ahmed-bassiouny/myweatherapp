package com.bassiouny.weatherapp.di

import com.bassiouny.network_module.NetworkExecutor
import com.bassiouny.network_module.weather.WeatherApi
import com.bassiouny.network_module.weather_network.WeatherNetwork
import com.bassiouny.network_module.weather_network.WeatherNetworkImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideWeatherNetwork(weatherApi: WeatherApi, networkExecutor: NetworkExecutor): WeatherNetwork =
        WeatherNetworkImpl(weatherApi,networkExecutor)



}