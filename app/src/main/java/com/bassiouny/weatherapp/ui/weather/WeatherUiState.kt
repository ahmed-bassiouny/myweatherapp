package com.bassiouny.weatherapp.ui.weather

import com.bassiouny.weatherapp.model.WeatherUIModel

data class WeatherUiState(
    val weatherUIModel: WeatherUIModel? = null,
    val isLoading: Boolean = false,
    val errorMessage: String = "",
)
