package com.bassiouny.weatherapp.ui.weather

enum class SearchWidgetState {
    OPENED,
    CLOSED
}