package com.bassiouny.weatherapp.ui.weather

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bassiouny.entity_module.weather.ForecastResponse
import com.bassiouny.user_case_module.weather.GetWeatherInfoNyLocation
import dagger.hilt.android.lifecycle.HiltViewModel
import com.bassiouny.user_case_module.location.LocationOnRequest
import com.bassiouny.user_case_module.location.LocationPermissionGranted
import com.bassiouny.user_case_module.UseCase
import com.bassiouny.user_case_module.weather.DEFAULT_WEATHER_DESTINATION
import com.bassiouny.user_case_module.weather.GetWeatherInfoNyName
import com.bassiouny.weatherapp.model.toWeather
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val getWeatherInfoNyName: GetWeatherInfoNyName,
    private val getWeatherInfoNyLocation: GetWeatherInfoNyLocation,
    private val locationPermissionGranted: LocationPermissionGranted,
    private val locationOnRequest: LocationOnRequest,
) : ViewModel() {

    private val _uiState: MutableStateFlow<WeatherUiState> =
        MutableStateFlow(WeatherUiState(isLoading = true))
    val uiState: StateFlow<WeatherUiState> = _uiState.asStateFlow()

    private val _searchWidgetState: MutableState<SearchWidgetState> =
        mutableStateOf(value = SearchWidgetState.CLOSED)
    val searchWidgetState: State<SearchWidgetState> = _searchWidgetState

    private val _searchTextState: MutableState<String> = mutableStateOf(value = "")
    val searchTextState: State<String> = _searchTextState

    private val _requestPermissionState: MutableState<Boolean> = mutableStateOf(false)
    val requestPermissionState: State<Boolean> = _requestPermissionState

    fun updateSearchWidgetState(newValue: SearchWidgetState) {
        _searchWidgetState.value = newValue
    }

    fun updateSearchTextState(newValue: String) {
        _searchTextState.value = newValue
    }

    init {
        viewModelScope.launch {
            if (locationPermissionGranted.execute(Unit))
                getWeatherByLocation()
            else {
                _requestPermissionState.value = true
            }
        }
    }

    fun getWeatherByLocation() {
        viewModelScope.launch {
            locationOnRequest.execute(Unit)?.let {
                getWeather(com.bassiouny.entity_module.user.UserLocation(it.latitude, it.longitude)) }
                ?: run { getWeather() }
        }
    }

    fun getWeather(city: String = DEFAULT_WEATHER_DESTINATION) {
        _uiState.value = WeatherUiState(isLoading = true)
        viewModelScope.launch {
            handleWeatherResponse(getWeatherInfoNyName.execute(city))
        }
    }

    fun getWeather(userLocation: com.bassiouny.entity_module.user.UserLocation) {
        _uiState.value = WeatherUiState(isLoading = true)
        viewModelScope.launch {
            handleWeatherResponse(getWeatherInfoNyLocation.execute(userLocation))
        }
    }

    private fun handleWeatherResponse(result: UseCase.Response<ForecastResponse>) {
        _uiState.value = WeatherUiState(isLoading = false)
        when (result) {
            is UseCase.Response.Success -> _uiState.value = WeatherUiState(weatherUIModel = result.value.toWeather())
            is UseCase.Response.Error -> _uiState.value =
                WeatherUiState(errorMessage = result.error)
        }
    }

}