package com.bassiouny.weatherapp.model

data class HourUIModel(
    val time: String,
    val icon: String,
    val temperature: String,
)
