package com.bassiouny.weatherapp.model

data class ForecastUIModel(
    val date: String,
    val maxTemp: String,
    val minTemp: String,
    val sunrise: String,
    val sunset: String,
    val icon: String,
    val hourUIModel: List<HourUIModel>,
    val day:String?
)
