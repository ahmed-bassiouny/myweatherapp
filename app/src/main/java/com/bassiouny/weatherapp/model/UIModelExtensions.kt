package com.bassiouny.weatherapp.model

import com.bassiouny.entity_module.weather.ForecastResponse
import com.bassiouny.weatherapp.utils.DateUtil.toFormattedDay


fun ForecastResponse.toWeather(): WeatherUIModel = WeatherUIModel(
    temperature = current.tempC.toInt(),
    temperatureF = current.tempF.toInt(),
    date = forecast.forecastday.firstOrNull()?.date ?: "",
    wind = current.windKph.toInt(),
    humidity = current.humidity,
    condition = current.condition,
    uv = current.uv.toInt(),
    name = location.name,
    forecastUIModels = forecast.forecastday.map { networkForecastday ->
        networkForecastday.toWeatherForecast()
    }
)

fun ForecastResponse.NetworkForecast.NetworkForecastday.toWeatherForecast(): ForecastUIModel =
    ForecastUIModel(
        date = date,
        maxTemp = day.maxtempC.toInt().toString(),
        minTemp = day.mintempC.toInt().toString(),
        sunrise = astro.sunrise,
        sunset = astro.sunset,
        icon = day.condition.icon,
        hourUIModel = hour.map { networkHour ->
            networkHour.toHour()
        },
        day = date.toFormattedDay()
    )

fun ForecastResponse.NetworkForecast.NetworkForecastday.NetworkHour.toHour(): HourUIModel =
    HourUIModel(
        time = time,
        icon = condition.icon,
        temperature = tempC.toInt().toString(),
    )