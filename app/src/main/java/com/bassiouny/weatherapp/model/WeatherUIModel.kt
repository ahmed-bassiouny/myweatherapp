package com.bassiouny.weatherapp.model

import com.bassiouny.entity_module.weather.ForecastResponse


data class WeatherUIModel(
    val temperature: Int,
    val temperatureF: Int,
    val date: String,
    val wind: Int,
    val humidity: Int,
    val condition: ForecastResponse.Current.Condition,
    val uv: Int,
    val name: String,
    val forecastUIModels: List<ForecastUIModel>
)
